import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ServicesComponent } from './services/services.component';
import { BookServiceComponent } from './book-service/book-service.component';
import { CustomerExperienceComponent } from './customer-experience/customer-experience.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from './/app-routing.module';
import { CarWashingComponent } from './car-washing/car-washing.component';
import { BikeWashingComponent } from './bike-washing/bike-washing.component';
import { PuncherRepairComponent } from './puncher-repair/puncher-repair.component';
import { PetrolDeliveredComponent } from './petrol-delivered/petrol-delivered.component';
// For MDB Angular Pro
// import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { CarouselModule, WavesModule, ButtonsModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ServicesComponent,
    BookServiceComponent,
    CustomerExperienceComponent,
    ContactUsComponent,
    HeaderComponent,
    FooterComponent,
    CarWashingComponent,
    BikeWashingComponent,
    PuncherRepairComponent,
    PetrolDeliveredComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    // CarouselModule,
    // WavesModule,
    // ButtonsModule,
    // MDBBootstrapModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
