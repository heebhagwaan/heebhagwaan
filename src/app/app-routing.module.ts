import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from '../app/about/about.component';
import { BookServiceComponent } from '../app/book-service/book-service.component';
import { ContactUsComponent } from '../app/contact-us/contact-us.component';
import { CustomerExperienceComponent } from '../app/customer-experience/customer-experience.component';
import { HomeComponent } from '../app/home/home.component';
import { ServicesComponent } from '../app/services/services.component';
import { CarWashingComponent } from '../app/car-washing/car-washing.component';
import { BikeWashingComponent } from '../app/bike-washing/bike-washing.component';
import { PetrolDeliveredComponent } from '../app/petrol-delivered/petrol-delivered.component';
import { PuncherRepairComponent } from  '../app/puncher-repair/puncher-repair.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'book-service', component: BookServiceComponent },
  { path: 'customer-review', component: CustomerExperienceComponent },
  { path: 'about', component: AboutComponent },
  { path: 'service', component: ServicesComponent },
  { path: 'car-washing', component: CarWashingComponent },
  { path: 'bike-washing', component: BikeWashingComponent },
  { path: 'petrol-delivered', component: PetrolDeliveredComponent },
  { path: 'puncher-repair', component: PuncherRepairComponent }
];

@NgModule({
  imports: [
    CommonModule,
     RouterModule.forRoot(routes)
  ],
   exports: [ RouterModule ],
   
  declarations: []
})
export class AppRoutingModule {
}

     //  HomeComponent ,ContactUsComponent,BookServiceComponent,CustomerExperienceComponent,
     // AboutComponent ,ServicesComponent ,CarWashingComponent ,BikeWashingComponent ,PetrolDeliveredComponent 
     // ,PuncherRepairComponent