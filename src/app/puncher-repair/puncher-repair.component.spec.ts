import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuncherRepairComponent } from './puncher-repair.component';

describe('PuncherRepairComponent', () => {
  let component: PuncherRepairComponent;
  let fixture: ComponentFixture<PuncherRepairComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuncherRepairComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuncherRepairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
