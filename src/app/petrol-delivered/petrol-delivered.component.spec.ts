import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetrolDeliveredComponent } from './petrol-delivered.component';

describe('PetrolDeliveredComponent', () => {
  let component: PetrolDeliveredComponent;
  let fixture: ComponentFixture<PetrolDeliveredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetrolDeliveredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetrolDeliveredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
