import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarWashingComponent } from './car-washing.component';

describe('CarWashingComponent', () => {
  let component: CarWashingComponent;
  let fixture: ComponentFixture<CarWashingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarWashingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarWashingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
