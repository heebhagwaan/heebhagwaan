import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeWashingComponent } from './bike-washing.component';

describe('BikeWashingComponent', () => {
  let component: BikeWashingComponent;
  let fixture: ComponentFixture<BikeWashingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikeWashingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikeWashingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
